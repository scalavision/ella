import showHideTrack from '../actions/showHideTrack'
import updateIgvTracks from '../actions/updateIgvTracks'

export default [showHideTrack, updateIgvTracks]
